package tbot

// Type of the entity. Can be mention (@username), hashtag, bot_command, url, email,
// bold (bold text), italic (italic text), code (monowidth string), pre (monowidth block),
// text_link (for clickable text URLs), text_mention (for users without usernames)

const (
	Mention 	= "mention"
	Hashtag 	= "hashtag"
	BotCommand 	= "bot_command"
	Url 		= "url"
	Bold 		= "bold"
	Italic 		= "italic"
	Code 		= "code"
	Pre 		= "pre"
	TextLink 	= "text_link"
	TextMention 	= "text_mention"
)

type MessageEntity struct {
	Type 	string
	Offset 	int
	Length 	int
	Url 	string
	User 	User
}
