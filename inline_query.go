package tbot

type InlineQuery struct {
	Id 	int	`json:"id"`
	From 	User	`json:"from"`
	Query 	string	`json:"query"`
	Offset 	string	`json:"offset"`
}
