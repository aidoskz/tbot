package tbot

type Update struct {
	UpdateId 		int 		`json:"update_id"`
	Message					`json:"message"`
	EditedMessage 		Message		`json:"edited_message"`
	InlineQuery				`json:"inline_query"`
	ChosenInlineResult			`json:"chosen_inline_result"`
	CallbackQuery				`json:"callback_query"`
}

func (u *Update) IsCallbackQuery() bool {
	return u.CallbackQuery.Id != ""
}
