package tbot

import "encoding/json"

type ReplyKeyboardHide struct {
	HideKeyboard 	bool 	`json:"hide_keyboard"`
	Selective 	bool 	`json:"selective"`
}

type replyKeyboardHideJSON ReplyKeyboardHide

func (rm ReplyKeyboardHide) MarshalJSON() ([]byte, error) {
	k := replyKeyboardHideJSON{
		HideKeyboard: 	rm.HideKeyboard,
		Selective: 	rm.Selective,
	}

	return json.Marshal(k)
}
