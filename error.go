package tbot

type SendError struct {
	ChatId 	int
	Msg	string
}

func (e *SendError) Error() string {
	return e.Msg
}
